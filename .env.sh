export PATH="$PATH:/opt/docs/bin"

mkdir -p /home/$USER/.local/bin
chown -R "$USER":"$GROUP" "/home/$USER/.local"
ln -sf /opt/docs/.venv/bin/mkdocs /home/$USER/.local/bin/mkdocs
