FROM ubuntu:bionic

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

ENV DEBIAN_FRONTEND "noninteractive"
ENV PYTHONVENV "/opt/docs/.venv"

RUN apt update && \
    apt install -y \
    python3 \
    python3-pip \
    python3-venv

RUN python3 -m venv "$PYTHONVENV"

RUN bash -c "source $PYTHONVENV/bin/activate && \
    pip3 install \
    mkdocs \
    mkdocs-material \
    mkdocs-awesome-pages-plugin \
    && rm -rf /root/.cache"

RUN ln -s /opt/docs/.venv/bin/mkdocs /usr/local/bin/mkdocs

COPY .env.sh /opt/docs/.env.sh
VOLUME /opt/docs

CMD ["/bin/sh", "-c", "trap 'exit 147' TERM; tail -f /dev/null & wait ${!}"]
